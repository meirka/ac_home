from lib2to3.pytree import Base
import re
from urllib.parse import urlsplit
from bs4 import BeautifulSoup

import requests
import timeout_decorator

PARSING_TIMEOUT = 10

@timeout_decorator.timeout(PARSING_TIMEOUT, use_signals=False, timeout_exception=TimeoutError)
def parse_page(url):

    try:

        # TODO: manage redirects

        # extract base url and path to resolve relative links
        parts = urlsplit(url)
        base_url = "{0.scheme}://{0.netloc}".format(parts)
        path = url[:url.rfind('/')+1] if '/' in parts.path else url

        response = requests.get(url)

        assert response.status_code == 200

        new_emails = list(set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I)))
        new_urls = []

        soup = BeautifulSoup(response.text, features="html.parser")

        # find and process all the anchors in the document
        for anchor in soup.find_all("a"):
            # extract link url from the anchor
            link = anchor.attrs["href"] if "href" in anchor.attrs else ''
            # add base url to relative links
            if link.startswith('/'):
                link = base_url + link 
            elif not link.startswith('http'):
                link = path + link

            if link.startswith('http'):
                new_urls.append(link)

        new_urls = list(set(new_urls))

        return base_url, new_emails, new_urls

    except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):

        return url, [], []

    except AssertionError:

        return url, [], []

    except BaseException as e:

        print(f"Failed to parse {url}\n -> {e}")

        return url, [], []