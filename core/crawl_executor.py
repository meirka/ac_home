from concurrent.futures import ThreadPoolExecutor
import networkx as nx
from core.page_utils import parse_page
from pathlib import Path

class CrawlExecutor:

    def __init__(self, crawler_seed: list, max_workers=4, graph_seed_path=None) -> None:
        
        self.executor = ThreadPoolExecutor(max_workers=max_workers)

        if graph_seed_path is None:

            self.graph = nx.DiGraph()
            for url in crawler_seed:
                self.graph.add_node(url, emails=set())

        else:

            self.load(graph_seed_path)


    def make_process_function(self, url):

        def process_single():

            try:
                try:
                    base_url, new_emails, new_urls = parse_page(url)
                    failure_status = None
                except TimeoutError:
                    print(f"{url} timed out")
                    # TODO: Log all errors in similar fashion to gather stats
                    base_url, new_emails, new_urls = url, [], []
                    failure_status = 'timeout'

                for new_url in new_urls:
                    self.graph.add_edge(url, new_url)

                self.graph.nodes[url]['emails'] = new_emails
                self.graph.nodes[url]['emails_num'] = len(new_emails)
                self.graph.nodes[url]['base_url'] = base_url
                self.graph.nodes[url]['failure'] = failure_status
                
            except BaseException as e:
                print(f"General failure on {url} -> {e}")
                return 0, 0
            
            return len(new_emails), len(new_urls)

        return process_single

    def make_graph_path(self, path, postfix):
        graph_path = Path(path) / f'link_graph{postfix}.gpickle'
        return graph_path

    def load(self, path, postfix=''):

        graph_path = self.make_graph_path(path, postfix)
        self.graph = nx.read_gpickle(graph_path)


    def persist(self, path, postfix=''):

        graph_path = self.make_graph_path(path, postfix)
        nx.write_gpickle(self.graph, graph_path)
    
    def process(self, iter_limit=None):

        if iter_limit is not None:
            iter_generator = range(iter_limit)
        else:
            iter_generator = iter(int, 1)

        for i in iter_generator:

            try:

                sink_nodes = [
                    node for node, outdegree
                    in self.graph.out_degree(self.graph.nodes()) if outdegree == 0
                ]

                print(f'Working {len(sink_nodes)} nodes')

                # We'll make concurrency batched. True, optimization might be achieved in general case
                # if executor is fed a queue of urls, continuously supplied by sinks from an updated graph 
                # from a separate thread, but we'll leave it for further versions
                
                futures = []

                for node in sink_nodes:
                    futures.append(self.executor.submit(self.make_process_function(node)))

                update_stats = tuple(map(sum, zip(*[f.result() for f in futures])))

                # update_stats = p_umap(
                #     self.process_single,
                #     sink_nodes
                # )

                print(f'    {update_stats[0]} new emails, {update_stats[1]} new urls')

            except KeyboardInterrupt:

                break

            finally:

                self.persist('./data', postfix=f'_on_{i}')