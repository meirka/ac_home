This project is a case study on email gathering website crawler capabilities.

1. Problem statement can be found in the Programming Challenge - Data Science Researcher.pdf file
2. ./core folder contains implementations of utilities written for the project
3. Part 1 of the study where crawler job is defined and the graph is built can be found in the 1 - Data origination.ipynb notebook
4. Part 2 of the study whre the graph is analised in an attempt to answer the stated problem can be found in the 2 - Graph analysis.ipynb notebook
5. Notebooks can be executed in an environment based on either one of the .yml conda env files in the repository
6. graph artifact originated in step 1 can be found at the link https://drive.google.com/drive/u/0/folders/11-dF412-AEUvCJJnvw-rQu2zITIRiqNz
